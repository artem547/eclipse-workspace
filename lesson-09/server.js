var express = require("express");
var bodyParser = require("body-parser");

var server = express();
var jsonParser = bodyParser.json();

server.use(express.static(__dirname));
server.use(jsonParser);

server.get("/", function(request, response){
	console.log('Start page is running');
	response.send("<h1>Welcome to the lesson regarding AJAX</h1>");
});

server.get("/userGet", function(request, response){
	console.log(request.query);
	response.send("Get method is succesfull: "+JSON.stringify(request.query));
})

server.post("/userPost", function(request, response){
	console.log(request.query);
	response.send("Get method is succesfull: "+JSON.stringify(request.body));
})

server.listen(3000);
